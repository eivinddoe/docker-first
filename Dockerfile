# Base Image
FROM python:3.7

# create and set working directory
RUN mkdir /app
WORKDIR /app

# add current directory code to working directory
ADD . /app/

# install system dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
	tzdata \
	python3-setuptools \
	python3-pip \
	python3-dev \
	python3-venv \
	git \
	&& \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/*

# install environment dependencies
RUN pip3 install --upgrade pip

# install project dependencies 
RUN pip3 install -r requirements.txt

EXPOSE 8888
CMD gunicorn firstdevops.wsgi:application --bind 0.0.0.0:8888